/* A packrat parser generated by PackCC 1.8.0 */

#ifndef PCC_INCLUDED_PARSER_H
#define PCC_INCLUDED_PARSER_H

#include "system.h"
#ifdef __cplusplus
extern "C" {
#endif

typedef struct parser_context_tag parser_context_t;

parser_context_t *parser_create(system_t *auxil);
int parser_parse(parser_context_t *ctx, ast_node_t **ret);
void parser_destroy(parser_context_t *ctx);

#ifdef __cplusplus
}
#endif

#endif /* !PCC_INCLUDED_PARSER_H */
