#include "select_menu.h"
// int select_menu(std::vector<std::string> choices)

#include "strconv2.h"

extern int main_calc5(int argc, const char **argv);
extern int main_json(int argc, const char **argv);

int main(int argc, const char **argv)
{
    std::vector<std::string> choices = { "calc5", "json" };
    int choice = select_menu(choices);
    unicode_ostream uout(std::cout);
    uout << "チョイス=" << choice << std::endl;
    switch(choice)
    {
    case 1:
        return main_calc5(argc, argv);
    case 2:
        return main_json(argc, argv);
    default:
        break;
    }

    return 0;
}
