// https://qiita.com/access3151fq/items/7b9e7d0de8c906f82a4c

import * as CSV from "https://deno.land/std/encoding/csv.ts";

// ↓直接pathに文字列を指定するとカレントディレクトリからの相対パス。import.meta.resolveを使用するとこのファイルからの相対パス。
const path = new URL(import.meta.resolve("./media.csv"));
const text = await Deno.readTextFile(path); // ファイルを文字列として読み取り
const data = CSV.parse(text); // 文字列をCSVとしてパース
console.log(data);
