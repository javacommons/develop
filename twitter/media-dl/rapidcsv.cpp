#include <iostream>
#include <vector>
#include "rapidcsv.h"
#include "strconv2.h"

int main()
{
  rapidcsv::Document doc("media.csv");

  //std::cout << doc.GetRowCount() << std::endl;
  for(size_t i=0; i<doc.GetRowCount(); i++)
  {
    // std::vector<T> GetRow(const size_t pRowIdx) const
    std::vector<std::string> row = doc.GetRow<std::string>(i);
    //std::cout << row.size() << std::endl;
    for(int j=0; j<row.size(); j++)
    {
      formatA(std::cout, "%s ", row[j].c_str());
    }
    formatA(std::cout, "\n");
  }

  return 0;
}