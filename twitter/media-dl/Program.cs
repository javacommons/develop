﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using CsvHelper;
using CsvHelper.Configuration;
using JavaCommons;
namespace CUI;

public class Foo
{
    public string TweetDate { get; set; }
    public string ActionDate { get; set; }
    public string DisplayName { get; set; }
    public string UserName { get; set; }
    public string TweetURL { get; set; }
    public string MediaType { get; set; }
    public string SavedFileName { get; set; }
    public string Remarks { get; set; }
    public string TweetContent { get; set; }
    public string Replies { get; set; }
    public string Retweets { get; set; }
    public string Likes { get; set; }
}

public static class Program
{
    [STAThread]
    public static void Main(string[] args)
    {
        Util.Print(Environment.Version.ToString(), "Runtime Version");
        Util.Print(Assembly.GetExecutingAssembly().GetName().Version.ToString(), "Assembly Version");
        Util.Print(args, "args");
        var config = new CsvConfiguration(CultureInfo.InvariantCulture)
        {
            HasHeaderRecord = false,
        };
        using (var reader = new StreamReader(@"D:\.repo\base\develop\twitter\media-dl\media.csv"))
        using (var csv = new CsvReader(reader, config))
        {
            var records = csv.GetRecords<Foo>();
            foreach (var record in records)
            {
                Util.Print(record);
                Util.Print(record.TweetDate);
            }

        }
    }
}
