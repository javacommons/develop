#! /usr/bin/env bash
set -uvx
set -e
rm -rf release debug
qmake-qt6.exe ./widget01.pro
make
cd release
windeployqt-qt6.exe ./widget01.exe
cp -p /mingw64/bin/libgcc_s_seh-1.dll .
cp -p /mingw64/bin/libstdc++-6.dll .
cp -p /mingw64/bin/libwinpthread-1.dll .
cp -p /mingw64/bin/libb2-1.dll .
cp -p /mingw64/bin/libdouble-conversion.dll .
cp -p /mingw64/bin/libicuin72.dll .
cp -p /mingw64/bin/libfreetype-6.dll .
cp -p /mingw64/bin/libharfbuzz-0.dll .
cp -p /mingw64/bin/libicuuc72.dll .
cp -p /mingw64/bin/libpcre2-16-0.dll .
cp -p /mingw64/bin/zlib1.dll .
cp -p /mingw64/bin/libmd4c.dll .
cp -p /mingw64/bin/libpng16-16.dll .
cp -p /mingw64/bin/libicudt72.dll .
cp -p /mingw64/bin/libbrotlidec.dll .
cp -p /mingw64/bin/libbz2-1.dll .
cp -p /mingw64/bin/libglib-2.0-0.dll .
cp -p /mingw64/bin/libgraphite2.dll .
cp -p /mingw64/bin/libintl-8.dll .
cp -p /mingw64/bin/libbrotlicommon.dll .
cp -p /mingw64/bin/libpcre2-8-0.dll .
cp -p /mingw64/bin/libiconv-2.dll .
