dofile('tostring.lua')
local Tbls = {
    {},
    {1, true, 3},
    {Foo = "Bar"},
    {["*Foo*"] = "Bar"},
    {Fnc},
    {"\0\1\0011\a\b\f\n\r\t\v\"\\"},
    {[{}] = {}},
    Tbl,
  }
print(tostring(Tbls))
