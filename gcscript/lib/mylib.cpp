﻿#include "mylib.h"

#include "omscript.h"
#include <gc.h>
//#include "windows.h"
#include "debug_line.h"

// malloc() の場合は, OOM Killer に kill される.
#define ARRAY_SIZE 10000
void c_test() {
    char* ary[ARRAY_SIZE];
    int i;
    for (i = 0; i < ARRAY_SIZE; i++)
        ary[i] = (char*) /*malloc(100000);*/ GC_MALLOC(100000);
    (void)ary[0];
}

extern "C" void parser_test()
{
    //GC_INIT();
    c_test();

    print(new_bool(true));
}
