#include <Windows.h>
#include <QtWidgets>

HHOOK hMouseHook;
QWidget *pos_label, *event_label;

LRESULT CALLBACK MouseHookProc(int nCode, WPARAM wParam, LPARAM lParam)
{
    if (nCode == HC_ACTION) {
        // マウスイベントの処理
        MSLLHOOKSTRUCT *pMSLLHook = reinterpret_cast<MSLLHOOKSTRUCT *>(lParam);
        if (wParam == WM_MOUSEMOVE) {
            // マウス移動イベントの処理
            int x = pMSLLHook->pt.x;
            int y = pMSLLHook->pt.y;
            pos_label->setProperty("text", QString("座標: (%1, %2)").arg(x).arg(y));
        } else if (wParam == WM_LBUTTONUP) {
            // 右ボタンクリックイベントの処理
            int y = pMSLLHook->pt.y;
            if (y < 1) {
                QMessageBox::information(nullptr, "メッセージ", "y座標が1未満です。");
            }
            QString text = "クリック: 左ボタン";
            if (GetKeyState(VK_CONTROL) < 0)
                text += " + Ctrl";
            if (GetKeyState(VK_SHIFT) < 0)
                text += " + Shift";
            if (GetKeyState(VK_MENU) < 0)
                text += " + Alt";
            event_label->setProperty("text", text);
        }
    }
    return CallNextHookEx(NULL, nCode, wParam, lParam);
}

class MouseTracker : public QWidget
{
public:
    MouseTracker(QWidget *parent = nullptr) : QWidget(parent)
    {
        // ラベルの初期化
        pos_label = new QLabel(this);
        event_label = new QLabel(this);

        // ラベルの位置とサイズの設定
        pos_label->setGeometry(20, 20, 150, 20);
        event_label->setGeometry(20, 50, 200, 20);

        // ウィンドウの初期設定
        setGeometry(300, 300, 240, 100);
        setWindowTitle("マウストラッカー");
        show();

        // マウスフックの設定
        hMouseHook = SetWindowsHookEx(WH_MOUSE_LL, MouseHookProc, NULL, 0);
    }

    ~MouseTracker()
    {
        // マウスフックの解除
        UnhookWindowsHookEx(hMouseHook);
    }
};

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MouseTracker tracker;
    return app.exec();
}
