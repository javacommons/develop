#include <QApplication>
#include <QMainWindow>
#include <QPushButton>
#include <iostream>
#include <thread>

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    Q_INVOKABLE void doSomething(int i)
    {
        std::cout << "Hello from main thread!: " << i << std::endl;
    }
};

void threadFunction(std::function<void(int)> callback)
{
    // スレッドで何らかの処理を行う
    // ...

    // コールバックを呼び出す
    callback(777);
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    MainWindow window;
    QPushButton button("Start Thread", &window);

    QObject::connect(&button, &QPushButton::clicked, [&]() {
        // スレッドを立ち上げて、コールバックを指定する
        new std::thread([&]() {
            threadFunction([&](int i) {
                // メインスレッドで何らかの処理を行う
                QMetaObject::invokeMethod(&window, "doSomething", Qt::QueuedConnection, Q_ARG(int, i));
            });
        });
    });

    window.setCentralWidget(&button);
    window.show();

    return app.exec();
}

#include "main.moc"
