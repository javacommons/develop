QT += core gui widgets
equals(QT_MAJOR_VERSION, 6):QT += core5compat

CONFIG += c++17
CONFIG += console
CONFIG += force_debug_info

#TEMPLATE = lib
#CONFIG += staticlib
#CONFIG += dll

#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000
DEFINES += DEBUG_LINE

gcc:QMAKE_CXXFLAGS_WARN_ON += -Wno-unused-parameter -Wno-unused-function -Wno-cast-function-type
msvc:QMAKE_CXXFLAGS += /bigobj

INCLUDEPATH += $$PWD

LIBS += -L$$[QT_INSTALL_PREFIX]/lib

DESTDIR = $$PWD

TARGET = $${TARGET}-$${QMAKE_HOST.arch}
#message($$QMAKE_QMAKE)
contains(QMAKE_QMAKE, .*static.*) {
    message( "[STATIC BUILD]" )
    DEFINES += QT_STATIC_BUILD
    TARGET = $${TARGET}-static
} else {
    message( "[SHARED BUILD]" )
}

include($$(HOME)/common/include/include.pri)
#include($$(HOME)/common/boost.pri)
#include($$(HOME)/common/common/common.pri)
#include($$(HOME)/common/common2/common2.pri)

include($$PWD/../lib/lib.pri)

HEADERS += utf8LogHandler.h \
    mal/core.h \
    mal/env.h \
    mal/libs/hashmap/hashmap.h \
    mal/libs/linked_list/linked_list.h \
    mal/printer.h \
    mal/reader.h \
    mal/types.h

SOURCES += \
    exe.cpp \
    mal/core.c \
    mal/env.c \
    mal/libs/hashmap/hashmap.c \
    mal/libs/linked_list/linked_list.c \
    mal/printer.c \
    mal/reader.c \
    mal/types.c

INCLUDEPATH += $$PWD/mal
LIBS += -lgc -lreadline
#DEFINES += WITH_FFI
#LIBS += -lffi -lltdl
