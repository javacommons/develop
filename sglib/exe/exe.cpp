﻿// This program sorts its parameters using
// insertion into sorted list (level 1 interface).
// For example:
//   a.out 6 7 3 4 1 5
// writes
//   1 3 4 5 6 7


#include <stdio.h>
#include <stdlib.h>
//#include <malloc.h>
#include <gc_util.h>
#include <string>
#include "sglib.h"

#include "strconv2.h"

using om_pointer = int;

typedef struct gcs_list {
    om_pointer p;
    struct gcs_list *next;
} gcs_list;
#define gcs_list_comparator(e1, e2) (e1->p - e2->p)
SGLIB_DEFINE_LIST_PROTOTYPES(gcs_list, gcs_list_comparator, next)
SGLIB_DEFINE_LIST_FUNCTIONS(gcs_list, gcs_list_comparator, next)

#if 0x0
#define GCS_HASH_TAB_SIZE 1024
typedef struct gcs_hash {
    std::string key;
    om_pointer p;
    struct gcs_hash *next;
} gcs_hash;
int gcs_hash_comparator(gcs_hash *e1, gcs_hash *e2)
{
    if (e1->key == e2->key) return 0;
    if (e1->key > e2->key) return 1;
    return -1;
}
unsigned int gcs_hash_function(gcs_hash *e)
{
    size_t hash = std::hash<std::string>()(e->key);
    return(hash % GCS_HASH_TAB_SIZE);
}
SGLIB_DEFINE_LIST_PROTOTYPES(gcs_hash, gcs_hash_comparator, next)
SGLIB_DEFINE_LIST_FUNCTIONS(gcs_hash, gcs_hash_comparator, next)
SGLIB_DEFINE_HASHED_CONTAINER_PROTOTYPES(gcs_hash, GCS_HASH_TAB_SIZE, gcs_hash_function)
SGLIB_DEFINE_HASHED_CONTAINER_FUNCTIONS(gcs_hash, GCS_HASH_TAB_SIZE, gcs_hash_function)
#endif


typedef struct gcs_rbtree {
    std::string key;
    om_pointer p;
    char color;
    struct gcs_rbtree *left;
    struct gcs_rbtree *right;
} gcs_rbtree;
int gcs_rbtree_comparator(gcs_rbtree *e1, gcs_rbtree *e2)
{
    if (e1->key == e2->key) return 0;
    if (e1->key > e2->key) return 1;
    return -1;
}
SGLIB_DEFINE_RBTREE_PROTOTYPES(gcs_rbtree, left, right, color, gcs_rbtree_comparator);
SGLIB_DEFINE_RBTREE_FUNCTIONS(gcs_rbtree, left, right, color, gcs_rbtree_comparator);


#if 0x0
void hash_test(int argc, char **argv)
{
    gcs_hash *htab[GCS_HASH_TAB_SIZE];
    int                                           i/*, n*/;
    struct gcs_hash                          /*ii, *nn,*/ *ll;
    struct sglib_hashed_gcs_hash_iterator    it;

    sglib_hashed_gcs_hash_init(htab);

    GC_PLACE(gcs_hash, ii, ());

    for (i=1; i<argc; i++) {
      //sscanf(argv[i],"%d", &n);
      ii->key = argv[i];
      if (sglib_hashed_gcs_hash_find_member(htab, ii) == NULL) {
          //nn = (gcs_hash *)GC_malloc(sizeof(struct gcs_hash));
          GC_PLACE(gcs_hash, nn, ());
          nn->key = argv[i];
          sglib_hashed_gcs_hash_add(htab, nn);
      }
    }

    for(ll=sglib_hashed_gcs_hash_it_init(&it,htab); ll!=NULL; ll=sglib_hashed_gcs_hash_it_next(&it)) {
        //printf("%s ", ll->key.c_str());
        format(std::cout, "漢字-%s ", ll->key.c_str());
    }
    format(std::cout, "\n");
}
#endif

void rbtree_test(int argc, char **argv)
{
    int                           i/*,a*/;
    struct gcs_rbtree                 /*e,*/ *t, *the_tree, *te;
    struct sglib_gcs_rbtree_iterator  it;

    the_tree = NULL;
    GC_PLACE(gcs_rbtree, e, ());
    for (i=1; i<argc; i++) {
      //sscanf(argv[i],"%d", &a);
      e->key = argv[i];
      if (sglib_gcs_rbtree_find_member(the_tree, e)==NULL) {
          //t = malloc(sizeof(struct rbtree));
          GC_PLACE(gcs_rbtree, t, ());
        t->key = argv[i];
        sglib_gcs_rbtree_add(&the_tree, t);
      }
    }

    for(te=sglib_gcs_rbtree_it_init_inorder(&it,the_tree); te!=NULL; te=sglib_gcs_rbtree_it_next(&it)) {
      format(std::cout, "%s ", te->key.c_str());
    }
    format(std::cout, "\n");
}


typedef struct ilist {
    int i;
    struct ilist *next_ptr;
} iListType;

#define ILIST_COMPARATOR(e1, e2) (e1->i - e2->i)

SGLIB_DEFINE_SORTED_LIST_PROTOTYPES(iListType, ILIST_COMPARATOR, next_ptr)
SGLIB_DEFINE_SORTED_LIST_FUNCTIONS(iListType, ILIST_COMPARATOR, next_ptr)

int main(int argc, char **argv) {
  int                               i,a;
  struct ilist                      *l, *the_list;
  struct sglib_iListType_iterator   it;
  the_list = NULL;
  for (i=1; i<argc; i++) {
    sscanf(argv[i],"%d", &a);
    l = (ilist *)malloc(sizeof(struct ilist));
    l->i = a;
    // insert the new element into the list while keeping it sorted
    sglib_iListType_add(&the_list, l);
  }
  // print the list
  for (l=the_list; l!=NULL; l=l->next_ptr) {
    format(std::cout, "%d ", l->i);
  }
  format(std::cout, "\n");
  // free all
  for(l=sglib_iListType_it_init(&it,the_list); l!=NULL; l=sglib_iListType_it_next(&it)) {
    free(l);
  }
  //hash_test(argc, argv);
  rbtree_test(argc, argv);
  return(0);
}
