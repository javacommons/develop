#include <iostream>

struct UIFrame
{
	SGS_OBJECT;

	UIFrame();

	SGS_METHOD void doMouseMove( float x, float y );
	SGS_METHOD void doMouseButton( int btn, bool down );
	SGS_METHOD void doKeyPress( int key, bool down );
	
	SGS_PROPERTY float x;
	SGS_PROPERTY float y;
	SGS_PROPERTY float width;
	SGS_PROPERTY float height;
    SGS_PROPERTY sgsString name;
	
	float prevMouseX;
	float prevMouseY;
};
