#include <irrlicht/irrlicht.h>
#include <iostream>

using namespace irr;

#pragma comment(lib, "Irrlicht.lib")

int main()
{
  video::E_DRIVER_TYPE driverType;

  driverType = video::EDT_SOFTWARE;

  IrrlichtDevice *device = createDevice(driverType,
                                        core::dimension2d<u32>(640, 480));

  if (device == 0)
    return 1;

  device->setWindowCaption(L"段階的学習スレ gamedev@2ch");

  video::IVideoDriver* driver = device->getVideoDriver();

  video::ITexture* images = driver->getTexture("all256.bmp");
  driver->makeColorKeyTexture(images, core::position2d<s32>(0,0));

  video::ITexture* imagesLogo = driver->getTexture("gamdev.bmp");

  //フォント設定
  gui::IGUIFont* font = device->getGUIEnvironment()->getBuiltInFont();

  //表示する画像の設定
  core::rect<s32> jiki1(0,24,15,39);
  core::rect<s32> jiki2(0,40,15,55);
  core::rect<s32> Logo(0,0,449,119);

  //メインループ
  while(device->run() && driver)
    {
      if (device->isWindowActive())
        {
          u32 time = device->getTimer()->getTime();

          driver->beginScene(true, true, video::SColor(0,120,102,136));

          // 自機を描く
          driver->draw2DImage(images, core::position2d<s32>(320,240),
                              (time/500 % 2) ? jiki1 : jiki2, 0, 
                              video::SColor(255,255,255,255), true);

          // テキスト表示
          if (font)
            font->draw(L"gamedev@2ch", 
                       core::rect<s32>(580,470,640,480),
                       video::SColor(255,255,255,255));

          //ロゴ表示
          driver->draw2DImage(imagesLogo, core::position2d<s32>(0,0),
                              Logo, 0, 
                              video::SColor(255,255,255,255), true);

          driver->endScene();
        }
    }

  device->drop();

  return 0;
}
