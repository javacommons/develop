// SGS/CPP-BC
// warning: do not modify this file, it may be regenerated during any build

#include "test.h"

static int _sgs_method__UIFrame__doMouseMove( SGS_CTX )
{
	UIFrame* data; if( !SGS_PARSE_METHOD( C, UIFrame::_sgs_interface, data, UIFrame, doMouseMove ) ) return 0;
	_sgsTmpChanger<sgs_Context*> _tmpchg( data->C, C );
	data->doMouseMove( sgs_GetVar<float>()(C,0), sgs_GetVar<float>()(C,1) ); return 0;
}

static int _sgs_method__UIFrame__doMouseButton( SGS_CTX )
{
	UIFrame* data; if( !SGS_PARSE_METHOD( C, UIFrame::_sgs_interface, data, UIFrame, doMouseButton ) ) return 0;
	_sgsTmpChanger<sgs_Context*> _tmpchg( data->C, C );
	data->doMouseButton( sgs_GetVar<int>()(C,0), sgs_GetVar<bool>()(C,1) ); return 0;
}

static int _sgs_method__UIFrame__doKeyPress( SGS_CTX )
{
	UIFrame* data; if( !SGS_PARSE_METHOD( C, UIFrame::_sgs_interface, data, UIFrame, doKeyPress ) ) return 0;
	_sgsTmpChanger<sgs_Context*> _tmpchg( data->C, C );
	data->doKeyPress( sgs_GetVar<int>()(C,0), sgs_GetVar<bool>()(C,1) ); return 0;
}

int UIFrame::_sgs_destruct( SGS_CTX, sgs_VarObj* obj )
{
	static_cast<UIFrame*>( obj->data )->C = C;
	static_cast<UIFrame*>( obj->data )->~UIFrame();
	return SGS_SUCCESS;
}

int UIFrame::_sgs_gcmark( SGS_CTX, sgs_VarObj* obj )
{
	_sgsTmpChanger<sgs_Context*> _tmpchg( static_cast<UIFrame*>( obj->data )->C, C );
	return SGS_SUCCESS;
}

int UIFrame::_sgs_getindex( SGS_ARGS_GETINDEXFUNC )
{
	_sgsTmpChanger<sgs_Context*> _tmpchg( static_cast<UIFrame*>( obj->data )->C, C );
	SGS_BEGIN_INDEXFUNC
		SGS_CASE( "x" ){ sgs_PushVar( C, static_cast<UIFrame*>( obj->data )->x ); return SGS_SUCCESS; }
		SGS_CASE( "y" ){ sgs_PushVar( C, static_cast<UIFrame*>( obj->data )->y ); return SGS_SUCCESS; }
		SGS_CASE( "width" ){ sgs_PushVar( C, static_cast<UIFrame*>( obj->data )->width ); return SGS_SUCCESS; }
		SGS_CASE( "height" ){ sgs_PushVar( C, static_cast<UIFrame*>( obj->data )->height ); return SGS_SUCCESS; }
		SGS_CASE( "name" ){ sgs_PushVar( C, static_cast<UIFrame*>( obj->data )->name ); return SGS_SUCCESS; }
	SGS_END_INDEXFUNC;
}

int UIFrame::_sgs_setindex( SGS_ARGS_SETINDEXFUNC )
{
	_sgsTmpChanger<sgs_Context*> _tmpchg( static_cast<UIFrame*>( obj->data )->C, C );
	SGS_BEGIN_INDEXFUNC
		SGS_CASE( "x" ){ static_cast<UIFrame*>( obj->data )->x = sgs_GetVar<float>()( C, 1 ); return SGS_SUCCESS; }
		SGS_CASE( "y" ){ static_cast<UIFrame*>( obj->data )->y = sgs_GetVar<float>()( C, 1 ); return SGS_SUCCESS; }
		SGS_CASE( "width" ){ static_cast<UIFrame*>( obj->data )->width = sgs_GetVar<float>()( C, 1 ); return SGS_SUCCESS; }
		SGS_CASE( "height" ){ static_cast<UIFrame*>( obj->data )->height = sgs_GetVar<float>()( C, 1 ); return SGS_SUCCESS; }
		SGS_CASE( "name" ){ static_cast<UIFrame*>( obj->data )->name = sgs_GetVar<sgsString>()( C, 1 ); return SGS_SUCCESS; }
	SGS_END_INDEXFUNC;
}

int UIFrame::_sgs_dump( SGS_CTX, sgs_VarObj* obj, int depth )
{
	_sgsTmpChanger<sgs_Context*> _tmpchg( static_cast<UIFrame*>( obj->data )->C, C );
	char bfr[ 39 ];
	sprintf( bfr, "UIFrame (%p) %s", obj->data, depth > 0 ? "\n{" : " ..." );
	sgs_PushString( C, bfr );
	if( depth > 0 )
	{
		{ sgs_PushString( C, "\nx = " ); sgs_DumpData( C, static_cast<UIFrame*>( obj->data )->x, depth ).push( C ); }
		{ sgs_PushString( C, "\ny = " ); sgs_DumpData( C, static_cast<UIFrame*>( obj->data )->y, depth ).push( C ); }
		{ sgs_PushString( C, "\nwidth = " ); sgs_DumpData( C, static_cast<UIFrame*>( obj->data )->width, depth ).push( C ); }
		{ sgs_PushString( C, "\nheight = " ); sgs_DumpData( C, static_cast<UIFrame*>( obj->data )->height, depth ).push( C ); }
		{ sgs_PushString( C, "\nname = " ); sgs_DumpData( C, static_cast<UIFrame*>( obj->data )->name, depth ).push( C ); }
		sgs_StringConcat( C, 10 );
		sgs_PadString( C );
		sgs_PushString( C, "\n}" );
		sgs_StringConcat( C, 3 );
	}
	return SGS_SUCCESS;
}

static sgs_RegFuncConst UIFrame__sgs_funcs[] =
{
	{ "doMouseMove", _sgs_method__UIFrame__doMouseMove },
	{ "doMouseButton", _sgs_method__UIFrame__doMouseButton },
	{ "doKeyPress", _sgs_method__UIFrame__doKeyPress },
	{ NULL, NULL },
};

static int UIFrame__sgs_ifn( SGS_CTX )
{
	sgs_CreateDict( C, NULL, 0 );
	sgs_StoreFuncConsts( C, sgs_StackItem( C, -1 ),
		UIFrame__sgs_funcs,
		-1, "UIFrame." );
	return 1;
}

static sgs_ObjInterface UIFrame__sgs_interface =
{
	"UIFrame",
	UIFrame::_sgs_destruct, UIFrame::_sgs_gcmark, UIFrame::_sgs_getindex, UIFrame::_sgs_setindex, NULL, NULL, UIFrame::_sgs_dump, NULL, NULL, NULL, 
};
_sgsInterface UIFrame::_sgs_interface(UIFrame__sgs_interface, UIFrame__sgs_ifn);

