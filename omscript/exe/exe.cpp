﻿#include "omscript.h"
#include "json.h"
#include <assert.h>

#include <cstdlib>
#include <iostream>
#include <string>
#include <peglib.h>
using namespace peg;

#include "debug_line.h"

std::string json_type(size_t x)
{
    switch(x)
    {
    case json_type_string: return "json_type_string";
    case json_type_number: return "json_type_number";
    case json_type_object: return "json_type_object";
    case json_type_array: return "json_type_array";
    case json_type_true: return "json_type_true";
    case json_type_false: return "json_type_false";
    case json_type_null: return "json_type_null";
    }
    return "?";
}

std::string json_text = R"***(
{
  "pi": 3.141,
  "happy": true,
  "name": "Niels",
  "nothing": null,
  "answer": {
    "everything": 42
  },
  "list": [1, 0, 2],
  "object": {
    "currency": "USD",
    "value": 42.99
  }
}
)***";

std::string json_peg = R"***(
        # JSON grammar based on RFC 4627 (http://www.ietf.org/rfc/rfc4627.txt)

        #json        <- object / array
        json        <- boolean / null / number / string / object / array / list / symbol

        object      <- '{' (member (','? member)*)? '}' { no_ast_opt }
        member      <- (symbol / string) ':' json

        array       <- '[' (json (','? json)*)? ']'

        list        <- '(' (json (','? json)* rest?)? ')'
        rest        <- ('.' json)

        boolean     <- 'false' / 'true'
        null        <- 'null' / 'nil'

        number      <- < minus int frac exp >
        minus       <- '-'?
        int         <- '0' / [1-9][0-9]*
        frac        <- ('.' [0-9]+)?
        exp         <- ([eE] [-+]? [0-9]+)?

        string      <- '"' < char* > '"'
        char        <- unescaped / escaped
        symbol      <- < '@'? [-+/*_=<>a-zA-Z0-9]+ >
        escaped     <- '\\' (["\\/bfnrt] / 'u' [a-fA-F0-9]{4})
        unescaped   <- [\u0020-\u0021\u0023-\u005b\u005d-\u10ffff]

        %whitespace <- [ \t\r\n]*
)***";

void *alloc_func(void *user_data, size_t size)
{
#ifdef OM_USE_GC
    return GC_malloc(size);
#else
    return malloc(size);
#endif
}

int main(int argc, char *argv[])
{
    //GC_INIT();
    om_pointer p = new_bool(true);
    debug_line();
    print(new_bool(true));
    print(new_dict({{"a", new_bool(false)}}));
    print(new_list({new_number(12.34), new_bool(false)}));
    //json j = json::parse(json_text);

    parser parser;
    parser.set_logger([](size_t line, size_t col, const std::string& msg, const std::string &rule) {
      std::cerr << line << ":" << col << ": " << msg << "\n";
    });
    parser.load_grammar(json_peg);
    parser.enable_ast();

    auto expr = R"***(
  { @abc_xyz: [11, 22, true, "abc\nxyz" nil] "aaa": (* (+ 123 456) x) "bbb": (cons1 . cons2) }
  )***";
    std::shared_ptr<Ast> ast;
    if (parser.parse(expr, ast)) {
      ast = parser.optimize_ast(ast);
      std::cout << ast_to_s(ast);
      std::cout << (*ast).name << std::endl;
      const auto &nodes = (*ast).nodes;
      //auto result = eval(*nodes[0]);
      for(std::size_t i=0; i<nodes.size(); i++)
      {
          std::cout << (*nodes[i]).name << std::endl;
      }

      //std::cout << expr << " = " << eval(*ast) << std::endl;
      return 0;
    }

    std::cout << "syntax error..." << std::endl;

    return 0;
}
