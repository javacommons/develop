#include <iostream>
#include <filesystem>
#include "strconv.h"

namespace fs = std::filesystem;

void print_space(const char *name, std::uintmax_t bytes)
{
    std::uintmax_t mega_bytes = bytes / (1024 * 1024);
    std::cout << name << " : " << bytes << "[B]"
              << " (" << mega_bytes << "[MB])" << std::endl;
}

int main()
{
    std::string s = U8("abc");
    fs::path p = "/";
    fs::space_info info = fs::space(p);

    std::cout << p << std::endl;
    print_space("capacity", info.capacity);
    print_space("free", info.free);
    print_space("available", info.available);
}
