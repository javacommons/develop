﻿#include "mylib.h"
#include "sgscript.h"

extern "C" __declspec(dllexport) void sgscript_test()
{
    SGS_CTX = sgs_CreateEngine(); // SGS_CTX is alias to `sgs_Context* C`
    sgs_Include( C, "fmt" );
    sgs_ExecFile( C, "serialize.sgs" );
    sgs_DestroyEngine( C );
}
