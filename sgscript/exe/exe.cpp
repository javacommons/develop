﻿//#include "sgscript.h"
#include <windows.h>
#include <iostream>

//extern "C" __declspec(dllimport) void sgscript_test();
typedef void (*sgscript_test_proto)();

int main(int argc, char *argv[])
{
    //if (app.arguments().size()<2) exit(1);
    //AddDllDirectory(LR"(D:\.repo\base\develop\sgscript\lib)");
    HMODULE h = LoadLibraryExW(LR"(D:\.repo\base\develop\sgscript\lib\lib5-x86_64.dll)",
                               NULL,
                               LOAD_LIBRARY_SEARCH_DLL_LOAD_DIR
                               );
    std::cout << h << std::endl;
    sgscript_test_proto sgscript_test = (sgscript_test_proto)GetProcAddress(h, "sgscript_test");
    sgscript_test();
#if 0x0
    SGS_CTX = sgs_CreateEngine(); // SGS_CTX is alias to `sgs_Context* C`
    sgs_Include( C, "fmt" );
    //sgs_ExecFile( C, "closure-counter.sgs" );
    sgs_ExecFile( C, "serialize.sgs" );
    sgs_DestroyEngine( C );
#endif
    return 0; // return app.exec();
}
