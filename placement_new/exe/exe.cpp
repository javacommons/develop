﻿#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include "utf8LogHandler.h"
#include "debug_line.h"

#include "mylib.h"

//#include <gc/gc.h>
#include "gc_util.h"

struct SomeClass
{
    SomeClass(const std::string &name)
        : name_(name)
    {
    }
    void hello()
    {
        std::cout << "hello " << name_ << "!" << std::endl;
    }
private:
    std::string name_;
};

#if 0x0
#define GC_PLACE(TYPE,VAR,ARGS) \
    TYPE *VAR = (TYPE *)GC_malloc(sizeof(TYPE)); \
    new (VAR) TYPE ARGS
#endif

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    qInstallMessageHandler(utf8LogHandler);
    qdebug_line1("Hello World!");
    qdebug_line1(MyLib::add2(11, 22));
    //SomeClass *j = (SomeClass *)GC_malloc(sizeof(SomeClass));
    //new (j) SomeClass();
    GC_PLACE(SomeClass, j, ("tom"));
    j->hello();
    return 0; // return app.exec();
}
