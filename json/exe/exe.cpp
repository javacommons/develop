﻿#include "json.h"
#include <assert.h>

#include <iostream>

#if 0x0
typedef enum json_type_e {
  json_type_string,
  json_type_number,
  json_type_object,
  json_type_array,
  json_type_true,
  json_type_false,
  json_type_null
} json_type_t;
#endif

std::string json_type(size_t x)
{
    switch(x)
    {
    case json_type_string: return "json_type_string";
    case json_type_number: return "json_type_number";
    case json_type_object: return "json_type_object";
    case json_type_array: return "json_type_array";
    case json_type_true: return "json_type_true";
    case json_type_false: return "json_type_false";
    case json_type_null: return "json_type_null";
    }
    return "?";
}

int main(int argc, char *argv[])
{
    const char json[] = "{\"a\" : true, \"b\" : [false, null, \"foo\"]}";
    struct json_value_s* root = json_parse(json, strlen(json));
    std::cout << json_type(root->type) << std::endl;
    assert(root->type == json_type_object);

    struct json_object_s* object = (struct json_object_s*)root->payload;
    std::cout << object->length << std::endl;
    assert(object->length == 2);

    struct json_object_element_s* a = object->start;

    struct json_string_s* a_name = a->name;
    std::cout << a_name->string << std::endl;
    assert(0 == strcmp(a_name->string, "a"));
    assert(a_name->string_size == strlen("a"));

    struct json_value_s* a_value = a->value;
    std::cout << json_type(a_value->type) << std::endl;
    assert(a_value->type == json_type_true);
    assert(a_value->type == json_type_true);
    assert(a_value->payload == NULL);

    struct json_object_element_s* b = a->next;
    assert(b->next == NULL);

    struct json_string_s* b_name = b->name;
    assert(0 == strcmp(b_name->string, "b"));
    assert(b_name->string_size == strlen("b"));

    struct json_value_s* b_value = b->value;
    std::cout << json_type(b_value->type) << std::endl;
    assert(b_value->type == json_type_array);

    struct json_array_s* array = (struct json_array_s*)b_value->payload;
    assert(array->length == 3);

    struct json_array_element_s* b_1st = array->start;

    struct json_value_s* b_1st_value = b_1st->value;
    assert(b_1st_value->type == json_type_false);
    assert(b_1st_value->payload == NULL);

    struct json_array_element_s* b_2nd = b_1st->next;

    struct json_value_s* b_2nd_value = b_2nd->value;
    assert(b_2nd_value->type == json_type_null);
    assert(b_2nd_value->payload == NULL);

    struct json_array_element_s* b_3rd = b_2nd->next;
    assert(b_3rd->next == NULL);

    struct json_value_s* b_3rd_value = b_3rd->value;
    assert(b_3rd_value->type == json_type_string);

    struct json_string_s* string = (struct json_string_s*)b_3rd_value->payload;
    assert(0 == strcmp(string->string, "foo"));
    assert(string->string_size == strlen("foo"));

    /* Don't forget to free the one allocation! */
    free(root);

    return 0;
}
